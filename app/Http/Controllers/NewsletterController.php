<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\APIRet;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return APIRet::ret('OK', '', [], \App\Models\Newsletter::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrInput = $request->only(['title','content']);
        $validator = Validator::make($arrInput ,[
                'title'=>'required',
                'content'=>'required'
        ]);
        if( $validator->fails() ){
            return APIRet::ret('FAILED', 'Error in provided fields', $validator->getMessageBag()->toArray());
        }       
        \App\Models\Newsletter::addNewsLetter($arrInput);

        
        return APIRet::ret('OK', 'Newsletter saved');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrInput = ['id'=>$id];
        $validator = Validator::make($arrInput ,[
                'id'=>'required|exists:newsletters'
        ]);
        if( $validator->fails() ){
            return APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        }           
        $ret = \App\Models\Newsletter::find($id);
        return APIRet::ret('OK', '', [], $ret);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arrInput = $request->only(['title','content','status']);
        $arrInput['id'] =$id;
        $validator = Validator::make($arrInput ,[
            'id'=>'required|exists:newsletters',
            'title'=>'required',
            'content'=>'required',
            'status' => 'required'
        ]);  
        if( $validator->fails() ){
            return \App\APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag(), $arrInput);
        }                
        \App\Models\Newsletter::updateNewsLetter($arrInput);

        return APIRet::ret('OK', 'Newsletter updated');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
