<?php

namespace App\Http\Middleware;

use Closure;

class APIAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiHeader = \App\APIHeader::get();

        $token = env('APP_KEY', 'uYVc3o39WoV79lcI4K3rcj7NUrO4l0wg');
        if( $apiHeader->appToken != $token ){
            return \App\APIRet::ret('FAILED', 'Unknown app token');
        }
        if( intval( $apiHeader->userType ) != 0 ){
            switch( $apiHeader->userType ){
                case 1: // financial advisers
                    $user = \App\Models\FinancialAdviser::find($apiHeader->userId);
                    if( $user ){
                        if( $user->remember_token !=  $apiHeader->apiToken ) {
                            return \App\APIRet::ret('FAILED', 'Not logged in');
                        }
                    }else{
                        return \App\APIRet::ret('FAILED', 'Unknown user');
                    }     
                    break;
                case 2: // end user
                    $user = \App\Models\EndUser::find($apiHeader->userId);
                    if( $user ){
                        if( $user->remember_token !=  $apiHeader->apiToken ) {
                            return \App\APIRet::ret('FAILED', 'Not logged in');
                        }
                    }else{
                        return \App\APIRet::ret('FAILED', 'Unknown user');
                    }               
                    break;
                case 3: // admins
                    $user = \App\User::find($apiHeader->userId);
                    if( $user ){
                        if( $user->remember_token !=  $apiHeader->apiToken ) {
                            return \App\APIRet::ret('FAILED', 'Not logged in');
                        }
                    }else{
                        return \App\APIRet::ret('FAILED', 'Unknown user');
                    }
                    break;
            }
        }
        
        return $next($request);
    }
}
