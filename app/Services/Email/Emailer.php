<?php

namespace App\Services\Email;
use Illuminate\Support\Facades\Log;

class Emailer{


	private $url="https://postal.parachure.com/api/v1/send/message";
	public function send($to, $subject, $from, $plain_body, $html_body="")
	{
		$toArr = [];
		 $toArr[] = $to;
		$objToSend = [
			"to"=> $toArr,
			"subject"=> $subject,
			"from"=> $from,
			"sender"=> $from,
			"plain_body"=> $plain_body,
			"html_body"=> $html_body
		];
		$strToSend = json_encode($objToSend);
		$ret = json_decode(self::sendPostRequest($this->url, $strToSend));
		return $ret;
	} 
	public static function getPEM()
	{
		$path = public_path() . '/../cacert.pem';
		return env('CERT_FILE', $path); 
	}
    public static function sendPostRequest($urlString, $postString)
	{
		$headers = array(
                        "Content-length:".strlen($postString),
                        'X-Server-API-Key: TBAoAW3Dne0orhTEhZnihqCS',
                        'Content-Type: application/json'
                    ); 

		$curl_connection = curl_init();
		curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($curl_connection, CURLOPT_URL, $urlString);
		curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($curl_connection, CURLOPT_SSL_CIPHER_LIST, 'DEFAULT:!DH');
		curl_setopt($curl_connection, CURLOPT_CAINFO, self::getPEM());		
		curl_setopt($curl_connection, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($curl_connection, CURLOPT_POST, true);
		curl_setopt($curl_connection, CURLOPT_HTTPHEADER, $headers);
		
		$result = curl_exec($curl_connection);
	
		if( $result === false )
		{
			$strErr = curl_error($curl_connection);
			Log::error("CURL:" . $strErr); 
			$result = '{
				"status": "curl_error"
			}';

		}
		curl_close($curl_connection);
		return $result;
	}   

};