<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table){
            $table->decimal('min_monthly_amt', 10,2);
            $table->decimal('max_monthly_amt', 10,2);
            $table->integer('min_term_in_months');
            $table->integer('max_term_in_months');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table){
            $table->dropColumn('min_monthly_amt');
            $table->dropColumn('max_monthly_amt');
            $table->dropColumn('min_term_in_months');
            $table->dropColumn('max_term_in_months');
        });        
    }
}
