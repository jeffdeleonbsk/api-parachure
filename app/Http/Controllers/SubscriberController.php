<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\APIRet;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return \App\APIRet::ret('OK', '', [], \App\Models\Subscriber::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        
        $arrInput = $request->only(['email']);        
        $validator = Validator::make($arrInput ,[
                'email'=>'required|email|unique:subscribers'
        ]);
        if( $validator->fails() ){
            return APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        }    
        $arrInput['name']= '';   
        $user = \App\Models\Subscriber::add($arrInput);

        $str = view('emails.subscribe', [])->render();
        
        $mailer = new \App\Services\Email\Emailer();
        $ret = $mailer->send( $user->email, "Thank you for Subscribing to our newsletter!", "info@parachure.com", '', $str);
        if( $ret->status == 'success'){
            $emailArr = [
                'table_name'=>'subscribers',
                'foreign_id'=>$user->id,
                'email_title'=>'Subscribe Email'
            ];
            \App\Models\EmailSent::add($emailArr);
        }


        return \App\APIRet::ret('OK', 'Subscriber saved'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrInput = ['id'=>$id];
        $validator = Validator::make($arrInput ,[
                'id'=>'required|exists:subscribers'
        ]);
        if( $validator->fails() ){
            return \App\APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        }           
        $ret = \App\Models\Subscriber::find($id);
        return \App\APIRet::ret('OK', '', [], $ret);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arrInput = $request->only(['email','name','status']);
        $arrInput['id'] =$id;
        $validator = Validator::make($arrInput ,[
            'id'=>'required|exists:subscribers',
            'email'=>'required|email',
            'name'=>'required',
            'status' => 'required'
        ]);  
        if( $validator->fails() ){
            return \App\APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag(), $arrInput);
        }                
        \App\Models\Subscriber::updateSubscriber($arrInput);

        return \App\APIRet::ret('OK', 'Subscriber updated');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
