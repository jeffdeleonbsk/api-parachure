<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    public $table="newsletters";
    public static function titleExists($title)
    {
    	$sub = self::where('title', $title)->first();
    	if( $sub ){
    		return true;
    	}
    	return false;
    }
    public static function addNewsLetter($arr)
    {

		$tosave = new Newsletter();
		$tosave->title = $arr['title'];
		$tosave->content = $arr['content'];
		$tosave->status = 0;
		$tosave->save();
    }    
    public static function updateNewsLetter($arr)
    {
    	$tosave =self::find($arr['id']);   
    	if( $tosave ){
    		$old_status = $tosave->status;
    		$tosave->title = $arr['title'];
    		$tosave->content = $arr['content'];
    		$tosave->status = $arr['status'];
    		$tosave->save();

    		if( $old_status != $tosave->status ){
     			StatusChangeHistory::record('newsletters', $tosave->id, $old_status, $tosave->status, 'changed on edit ');
    		}
    	}
    }   

    public function changeStatus($status)
    {
    	$old_status = $this->status;
    	$this->status = $status;
    	$this->save();
    	StatusChangeHistory::record('newsletters', $this->id, $old_status, $this->status, 'changed alone ');
    }

}
