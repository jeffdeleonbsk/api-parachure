<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewsLetter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->longtext('content');
            $table->integer('status')->default(0);
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->timestamps();
        });
        Schema::create('subscribers', function(Blueprint $table){
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('name');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
        Schema::create('newsletter_subscriber', function(Blueprint $table){
            $table->increments('id');            
            $table->integer('newsletter_id');
            $table->integer('subscriber_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
        Schema::dropIfExists('subscribers');
        Schema::dropIfExists('newsletter_subscriber');
    }
}
