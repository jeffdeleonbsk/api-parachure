<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFEU extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa_eu', function(Blueprint $table){
            $table->increments('id');
            $table->integer('fa_id');
            $table->integer('eu_id');
            $table->integer('engagement_status');
            $table->timestamps();
        });
        Schema::create('fa_eu_engagement_history', function(Blueprint $table){
            $table->increments('id');
            $table->integer('fa_eu_id');
            $table->integer('fa_id');
            $table->integer('eu_id');
            $table->integer('old_engagement_status');
            $table->integer('new_engagement_status');            
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->string('comment');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fa_eu');
        Schema::dropIfExists('fa_eu_engagement_history');
    }
}
