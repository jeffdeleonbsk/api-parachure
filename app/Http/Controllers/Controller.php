<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Services\TestService;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    private $content = null;
    private function getDebtForMonth($month){
    	$total = 0;
    	foreach( $this->content->debts as $debt){
    		if( $debt->months_remaining >= $month ){
    			$total += $debt->monthly_amount;
    		}
    	}
    	return $total;
    }

    public function test(Request $request)
    {

    	$content = $request->getContent();
        $this->content = json_decode($content);

      //   "income":50000,
    		// "expenses":35000,
    		// "months_to_60": 360,
    		// "inflation": 6,
    		// "interest": 8,
    	$monthly_interest = log10( 1 + ($this->content->interest)/100.0) / 12.0;
    	$monthly_interest = pow(10, $monthly_interest);
    	$annual_inflation = 1 + ($this->content->inflation)/100.0;
		$monthly_inflation = log10( $annual_inflation ) / 12.0;
    	$monthly_inflation = pow(10, $monthly_inflation);
    	$out = [];

    	$year = 1;
    	$month = 1;
    	$expenses =  $this->content->expenses;
    	$income =  $this->content->income;
    	$mf = 0;
    	for( $ind = 1 ; $ind <= $this->content->months_to_60 ; $ind++){
    		if( $month > 12){
    			$year += 1;
    			$month = 1;
    			$expenses = $expenses * $annual_inflation;
    			$income = $income * $annual_inflation;
    		}
    		$debt = $this->getDebtForMonth($ind);

    		$disposable = $income-($expenses+$debt);
    		$old_mf = $mf;
    		$new_mf = $mf * $monthly_interest;
    		$rev_from_interest = $new_mf - $old_mf;
    		$mf = $new_mf + $disposable;
    		
    		$out[] = [
    			'year' => $year,
    			'month' => $month,
    			'income' =>$income,
    			'expenses' => $expenses,
    			'debt' => $debt,
    			'disposable'=>$disposable,
    			'mf'=>$mf,
    			'rev_from_interest'=>$rev_from_interest
    		];
    		$month++;

    	}
    	return $out;
    	    	

    }
}
