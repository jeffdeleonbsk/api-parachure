<?php
namespace App;

class APIRet{
	
	public static function ret($status='OK', $message='', $errors=[], $data=[], $redirect_url='' ){
		$longMessage = $message;
		if( count($errors) ){
			foreach($errors as $err ){
				$longMessage .= "\r\n" . $err;	
			}			 
		}
		return [
			'status'=>$status, 
			'message'=>$message, 
			'longMessage'=>$longMessage ,
			'errors'=>$errors, 
			'data'=>$data,
			'redirect_url'=>$redirect_url 
		];
	}
	public static function retMsgBag($status='OK', $message='', $msgBag = null, $data=[], $redirect_url=''){
		$errors = [];
		if( $msgBag ){
			$arr = $msgBag->toArray();
			foreach ($arr as $key => $value) {
				foreach($value as $val){
					$errors[] = $val;
				}
			}
		}
		return self::ret($status, $message, $errors, $data, $redirect_url);
	}

};