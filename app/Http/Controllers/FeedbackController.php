<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\APIRet;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\APIRet::ret('OK', '', [], \App\Models\Feedback::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $arrInput = $request->only(['email','comment', 'subject']);
        $validator = Validator::make($arrInput ,[
                'email'=>'required|email',
                'comment'=>'required',
                'subject'=>'required',
        ]);
        if( $validator->fails() ){
            return APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        }       
        \App\Models\Feedback::add($arrInput);
        return \App\APIRet::ret('OK', 'Feedback saved'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrInput = ['id'=>$id];
        $validator = Validator::make($arrInput ,[
                'id'=>'required|exists:feedbacks'
        ]);
        if( $validator->fails() ){
            return \App\APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        }           
        $ret = \App\Models\Feedback::find($id);
        return \App\APIRet::ret('OK', '', [], $ret);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        return \App\APIRet::ret('FAILED', 'Unsupported Action');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
