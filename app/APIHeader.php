<?php
namespace App;
use Request;
class APIHeader{
	
	public static function get(){

		$ret = new \stdClass();
		$ret->appToken = Request::header('App-Token');
		$ret->apiToken = Request::header('Api-Token');
		$ret->userId = intval(Request::header('UserId'));
		$ret->userType = intval(Request::header('UserType'));
		return $ret;
	}

};