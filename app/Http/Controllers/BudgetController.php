<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\APIRet;
use App\Models\Budget;
class BudgetController extends Controller
{
    public function compute(Request $request){

    	$inp = json_decode($request->inp);
    //	try{
	    	$ret = Budget::compute($inp);
	    	return APIRet::ret("OK", "Computed", [], $ret );
    	//}catch(\Exception $exc){
    	//	return APIRet::ret("FAILED", "Exception in input", [ $exc->getMessage()] );
    	//}

    }
}
