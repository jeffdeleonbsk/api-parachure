<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequency_types', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('status');
            $table->timestamps();

        });
         Schema::table('subscription_types', function(Blueprint $table){
            $table->decimal('subscription_fee', 10,2);
            $table->integer('frequency_id')->default(1);
        });
        Schema::create('subscription_type_history', function(Blueprint $table){
            $table->increments('id');

            $table->integer('subscription_type_id');
            $table->string('old_name');
            $table->text('old_description');
            $table->integer('old_status');
            $table->integer('old_entity_type_id');
            $table->decimal('old_subscription_fee', 10,2);
            $table->integer('old_frequency_id')->default(1);            

            $table->string('new_name');
            $table->text('new_description');
            $table->integer('new_status');
            $table->integer('new_entity_type_id');
            $table->decimal('new_subscription_fee', 10,2);
            $table->integer('new_frequency_id')->default(1);              

            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->string('comment');


            $table->timestamps();
        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequency_types');
        Schema::dropIfExists('subscription_type_history');
        Schema::table('subscription_types', function(Blueprint $table){
            $table->dropColumn('subscription_fee');
            $table->dropColumn('frequency_id');
        });
    }
}
