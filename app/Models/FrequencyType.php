<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FrequencyType extends Model
{
    public $table = "frequency_types";
}
