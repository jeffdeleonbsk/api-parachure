<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionFA extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('financial_advisers', function(Blueprint $table){
            $table->integer('subscription_type_id');
        });
        Schema::create('fa_subscription_history', function(Blueprint $table){
            $table->increments('id');
            $table->integer('fa_id');
            $table->integer('old_subscription_type_id');
            $table->integer('new_subscription_type_id');

            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->string('comment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('financial_advisers', function(Blueprint $table){
            $table->dropColumn('subscription_type_id');
        });
        Schema::dropIfExists('fa_subscription_history');
    }
}
