<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailSent extends Model
{
    public $table="email_sent";
    public static function add($arr)
    {
        $tosave = new EmailSent();
        $tosave->table_name = $arr['table_name'];
        $tosave->foreign_id = $arr['foreign_id'];
        $tosave->email_title = $arr['email_title'];
        $tosave->save();  
    }
}
