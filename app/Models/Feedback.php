<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public $table = "feedbacks";

    public static function add($arr, $foreign = 0, $table_name = '')
    {
    	$tosave = new Feedback();
    	$tosave->foreign_id = $foreign;
    	$tosave->table_name =$table_name;
    	$tosave->comment = $arr['comment'];
        $tosave->subject = $arr['subject'];
    	$tosave->email = $arr['email'];
    	$tosave->status = 1;
    	$tosave->save();
    }
    public static function put($arr, $foreign = 0, $table_name = '')
    {
    	$tosave = Feedback::find($arr['id']);
    	$tosave->foreign_id = $foreign;
    	$tosave->table_name =$table_name;
    	$tosave->comment = $arr['comment'];
        $tosave->subject = $arr['subject'];
    	$tosave->email = $arr['email'];
    	$tosave->status = 1;
    	$tosave->save();
    }

}
