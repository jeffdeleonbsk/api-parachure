<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTypeHistory extends Model
{
    public $table = "subscription_type_history";
}
