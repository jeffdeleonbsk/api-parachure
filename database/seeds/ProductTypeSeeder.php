<?php

use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$toSeed = [
    		'product_types'=>[
    			['id'=>1,'status'=>1,'name'=>'Mutual Funds'	]
			],
  		];
  		foreach( $toSeed as $key=>$value){
  			DB::table($key)->truncate();
  			foreach($value as $row){
    			\DB::table($key)->insert($row);
    		}
  		}
    }
}
