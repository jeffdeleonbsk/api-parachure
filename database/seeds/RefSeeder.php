<?php

use Illuminate\Database\Seeder;

class RefSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  		$toSeed = [
  			'image_types'=>[
    		['id'=>1,'status'=>1,'name'=>'Banner'	],
    		['id'=>2,'status'=>1,'name'=>'Logo'	],
    		['id'=>3,'status'=>1,'name'=>'Product Images'	],
    		['id'=>4,'status'=>1,'name'=>'Video'	],
    		],
    		'contact_types'=>[
    		['id'=>1,'status'=>1,'name'=>'Email'	],
    		['id'=>2,'status'=>1,'name'=>'Phone'	],
    		['id'=>3,'status'=>1,'name'=>'Fax'	]
    		]
  		];
  		foreach( $toSeed as $key=>$value){
  			DB::table($key)->truncate();
  			foreach($value as $row){
    			\DB::table($key)->insert($row);
    		}
  		}
    }
}
