<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EndUser extends Model
{
    public $table = "end_users";
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function add($arr)
    {
 		$tosave = new EndUser();
		$tosave->email = $arr['email'];
		$tosave->password = bcrypt($arr['password']);
		if(isset($arr['first_name'])) $tosave->first_name = $arr['first_name'];
		if(isset($arr['last_name'])) $tosave->last_name = $arr['last_name'];
		$tosave->status = 1;
		$tosave->save();   	
        return $tosave;
    }
}
