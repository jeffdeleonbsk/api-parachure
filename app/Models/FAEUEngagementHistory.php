<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FAEUEngagementHistory extends Model
{
    public $table='fa_eu_engagement_history';
}
