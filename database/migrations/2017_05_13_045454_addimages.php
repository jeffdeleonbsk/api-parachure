<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addimages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_type_id');
            $table->integer('image_type_id');
            $table->integer('foreign_id');
            $table->string('image_url')->default('');
            $table->string('thumb_url')->default('');
            $table->integer('sort_priority')->default(0);
            $table->integer('status')->default(0);
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->timestamps();
        });  
        Schema::create('contacts', function(Blueprint $table){
            $table->increments('id');
            $table->integer('entity_type_id');
            $table->integer('foreign_id');
            $table->integer('contact_type_id');
            $table->string('contact')->default('');
            $table->integer('sort_priority')->default(0);
            $table->integer('status')->default(0);
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->timestamps();
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
        Schema::dropIfExists('contacts');
    }
}
