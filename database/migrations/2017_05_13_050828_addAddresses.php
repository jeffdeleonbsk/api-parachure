<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
        "id":1,
    "entity_type_id": 1, // "financial_advvisors"
    "foreign_id": 1, // foreign key,
    "address_1": "",
    "address_2": "",
    "address_3": "",
    "city": "",
    "state_province": "",
    "country": "",
    "sort_priority": 0, // negative means it's less priority 
    "address_type": "Home", //Home or office or other addresses
     */
    public function up()
    {
        Schema::create('addresses', function(Blueprint $table){
            $table->increments('id');
            $table->integer('entity_type_id');
            $table->integer('foreign_id');
            $table->integer('address_type_id');
            $table->string('address_1')->default('');
            $table->string('address_2')->default('');
            $table->string('address_3')->default('');
            $table->string('city')->default('');
            $table->string('country')->default('');
            $table->string('state_province')->default('');
            $table->string('zip')->default('');
            $table->integer('sort_priority')->default(0);
            $table->integer('status')->default(0);
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
