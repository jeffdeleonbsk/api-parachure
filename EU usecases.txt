
Important entities
FA - Financial Adviser
FO - Financial Organization
FP - Financial Product
MF - Mutual Fund
EA - Expert Adviser

EU - End User
MP - Main Portfolio
AP - Alternate Portfolio

CA - Common Admin
SA - Super Admin

CP - Cron Process 

Use Cases
EU
1.  EU can signup using FaceBook or regular signup
2.  EU can login 
3.  EU can edit his profile
6.  EU can add FP (choose from list of all FO)
7.  EU can remove FP from his list


9.  EU can create/edit MP
10. EU can create/edit/delete AP
12. EU can send notice to FA to be places under FA
13. EU can confirm/deny if FA wants to soft-edit his SP
14. EU can share AP with FA.



RULES:
1.  EU must be confirmed user before he can create MP
2.  EU must be confirmed user before he can send notice to FA
3.  EU can have multiple FA
4.  EU can have multiple AP
5.  EU can only have one MP
