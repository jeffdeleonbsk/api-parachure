<?php

namespace App\Services\Diagnostic;

abstract class AbstractCalculator 
{
    private $input = null;
    private $output = null;
    public function setInput($input){
    	$this->input = $input;
    }
    public function getOutput($recalculate = false){
    	if( $recalculate  || $this->output === null ){
    		$this->calculate();
    	}
    	return $this->output;
    }

    public function calculate();
}
