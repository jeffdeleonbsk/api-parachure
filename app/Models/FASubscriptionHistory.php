<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FASubscriptionHistory extends Model
{
    public $table='fa_subscription_history';
}
