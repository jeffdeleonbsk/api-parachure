<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public $table="subscribers";
    public static function emailExists($email)
    {
    	$sub = self::where('email', $email)->first();
    	if( $sub ){
    		return true;
    	}
    	return false;
    }
    public static function add($arr)
    {
 		$tosave = new Subscriber();
		$tosave->email = $arr['email'];
		$tosave->name = $arr['name'];
		$tosave->status = 1;
		$tosave->save();  
        return $tosave; 	
    }
    public static function updateSubscriber($arr)
    {
    	$tosave =self::find($arr['id']);   
    	if( $tosave ){
    		$old_status = $tosave->status;
    		$tosave->name = $arr['name'];
    		$tosave->email = $arr['email'];
    		$tosave->status = $arr['status'];
    		$tosave->save();

    		if( $old_status != $tosave->status ){
     			StatusChangeHistory::record( $this->table, $tosave->id, $old_status, $tosave->status, 'changed on edit ');
    		}
    	}
        return $tosave;
    }    
    public static function deactivateEmail($email)
    {
    	$sub = self::where('email', $email)->first();

    	if( $sub )
    		$sub->deactivate();
    }
    public function deactivate()
    {
    	$old_status = $this->status;
    	$this->status = 0;
    	$this->save();
    	StatusChangeHistory::record('subscribers', $this->id, $old_status, $this->status, 'Deactivated ');
    }
}
