<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\APIRet;

class SeederController extends Controller
{
    public function seed(\Illuminate\Http\Request $request){
 
    	// if( $request->headers['api-token'] != "abc123" )
    	// {
    	// 	return \App\APIRet::ret('FAIL', 'Wrong Token', [], $request->headers);
    	// }
    	$content = $request->getContent();
        $toSeed = json_decode($content, true);

        
        $tables = [];
  		foreach( $toSeed as $trow){
  			$table_name = $trow['name'];
  			\DB::table($table_name)->truncate();
  			foreach($trow['data'] as $row){
    			\DB::table($table_name)->insert($row);
    		}
    		$tables[] = $table_name;
  		}   
  		return \App\APIRet::ret('OK', '', [], $tables);     
    	
    }
    public function getSeeds()
    {
    	$tables = [
    		'image_types',
    		'entity_types',
    		'product_types',
    		'contact_types',
    		'address_types'
    	];
    	$ret = [];
    	foreach($tables as $table){
    		
    		$entries = \DB::table($table)->get();
    		$ret[]=[
    			"name"=> $table,
    			"data"=>$entries
    		];
    	}
    	return $ret;
    }
}
