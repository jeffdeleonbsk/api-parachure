<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignupSource extends Model
{
    public $table="signup_source";
    protected $fillable=['table_name', 'foreign_id','source','is_succesful'];
}
