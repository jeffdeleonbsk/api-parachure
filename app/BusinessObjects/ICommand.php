<?php
namespace App\BusinessObjects;
interface ICommand{
	public function execute(\Illuminate\Http\Request $request, $params);
}