<?php

namespace App\BusinessObjects

class NotYetImplemented implements ICommand{
	public function execute(\Illuminate\Http\Request $request, $params)
	{
		$errors = [];
		$message = "Command is not implemented yet";
		if(isset($params['errors'])) 
			$errors = $params['errors'];
		if(isset($params['message']))
			$message = $params['message'];
		return [
			"status"=>"FAILED", 
			"message"=>$message, 
			"errors"=>$errors,
			"data"=>[]
		];
	}
}