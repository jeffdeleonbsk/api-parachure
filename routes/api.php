<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('test', 'Controller@test');
Route::post('/seed', 'SeederController@seed');
Route::get('/seeds', 'SeederController@getSeeds');

// Route::post('/subscribers', 'NewsletterController@addSubscriber');
// Route::get('/subscribers', 'NewsletterController@getAllSubscriber');
// Route::get('/subscriber/{id}', 'NewsletterController@getSubscriber');
// Route::post('/subscriber/{id}', 'NewsletterController@addSubscriber');


// Route::post('/subscribers/deactivate', 'NewsletterController@deactivateSubscriber');

Route::post('compute', 'BudgetController@compute');
Route::post('signup', 'SignupController@signup');

Route::resource('newsletters', 'NewsletterController');
Route::resource('subscribers', 'SubscriberController');
Route::resource('feedback', 'FeedbackController');


