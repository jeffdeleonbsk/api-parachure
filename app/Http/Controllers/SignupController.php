<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\APIRet;

class SignupController extends Controller
{
    private function signupFA(Request $request)
    {
        $source = 'landingpage';
        if( $request->source ){
            $source = $request->source;
        }    
        $arrInput = $request->only(['email','password', 'first_name', 'last_name']);
        $validator = Validator::make($arrInput ,[
                'email'=>'required|email|unique:financial_advisers',
                'password'=>'required',
                'first_name'=>'required',
                'last_name'=>'required'
        ]);
        if( $validator->fails() ){            
            $arr = [
                'table_name'=> 'end_users',
                'foreign_id'=> 0,
                'source'=>$source,
                'is_succesful'=>0
            ];
            \App\Models\SignupSource::create($arr);
            return APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        } 
        $user = \App\Models\FinancialAdviser::add($arrInput);
        if( isset( $request->budget) ){
            $budgetArr = [
                'table_name'=>'financial_advisers',
                'foreign_id'=>$user->id,
                'name'=>'Default',
                'json_content'=>$request->budget
            ];
            \App\Models\Budget::addBudget($budgetArr);
        }        
        $str = view('emails.signupfa', [
            'user' =>  $user,
            'password' => $arrInput['password']
        ])->render();

        $mailer = new \App\Services\Email\Emailer();
        $ret = $mailer->send( $user->email, "Thank you for signing up!", "info@parachure.com", '', $str);
        if( $ret->status == 'success'){
            $emailArr = [
                'table_name'=>'financial_advisers',
                'foreign_id'=>$user->id,
                'email_title'=>'Signup Email'
            ];
            \App\Models\EmailSent::add($emailArr);
        } 
        $arr = [
            'table_name'=> 'end_users',
            'foreign_id'=> $user->id,
            'source'=>$source,
            'is_succesful'=>1
        ];
        \App\Models\SignupSource::create($arr);

        return \App\APIRet::ret('OK', 'Financial Adviser saved',[json_encode($ret)],$user);         
    }

    private function signupEU(Request $request)
    {   
        $source = 'landingpage';
        if( $request->source ){
            $source = $request->source;
        }        
        $arrInput = $request->only(['email','password', 'first_name', 'last_name']);
        $validator = Validator::make($arrInput ,[
                'email'=>'required|email|unique:end_users',
                'password'=>'required|min:6',
                'first_name'=>'required'
        ]);
        if( $validator->fails() ){
            $arr = [
                'table_name'=> 'end_users',
                'foreign_id'=> 0,
                'source'=>$source,
                'is_succesful'=>0
            ];
            \App\Models\SignupSource::create($arr);
            return APIRet::retMsgBag('FAILED', 'Error in provided fields', $validator->getMessageBag());
        } 

        $user = \App\Models\EndUser::add($arrInput);
        if( isset( $request->budget) ){
            $budgetArr = [
                'table_name'=>'end_users',
                'foreign_id'=>$user->id,
                'name'=>'Default',
                'json_content'=>$request->budget
            ];
            \App\Models\Budget::addBudget($budgetArr);
        }
        $str = view('emails.signupeu', [
            'user' =>  $user,
            'password' => $arrInput['password']
        ])->render();
        
        $mailer = new \App\Services\Email\Emailer();
        $ret = $mailer->send( $user->email, "Thank you for signing up!", "info@parachure.com", '', $str);
        if( $ret->status == 'success'){
            $emailArr = [
                'table_name'=>'financial_advisers',
                'foreign_id'=>$user->id,
                'email_title'=>'Signup Email'
            ];
            \App\Models\EmailSent::add($emailArr);
        }

        $arr = [
            'table_name'=> 'end_users',
            'foreign_id'=> $user->id,
            'source'=>$source,
            'is_succesful'=>1
        ];
        \App\Models\SignupSource::create($arr);
        return \App\APIRet::ret('OK', 'User saved',[$ret->status], $user);     	
    }    
    public function signup(Request $request)
    {   
        try{
            $isFA = ($request->isFA == 'true');        
            $userType = $isFA?1:2;      

            switch($userType){
                case 1: // FA
                    return $this->signupFA($request);
                case 2: // EU
                    return $this->signupEU($request);
            }            
        }catch(\Exception $exc){
            return \App\APIRet::ret('FAILED', $exc->getMessage());
        }

    }  



}
