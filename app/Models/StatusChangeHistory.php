<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusChangeHistory extends Model
{
    public $table="status_change_history";

    public static function record($table_name, $foreign_id, $old_status, $new_status, $comment )
    {
    	$sch = new StatusChangeHistory();
    	$sch->table_name = $table_name;
    	$sch->foreign_id = $foreign_id;
    	$sch->old_status = $old_status;
    	$sch->new_status = $new_status;
    	$sch->comment = $comment;
    	$sch->save();
    }
}
/*
        Schema::create('status_change_history', function(Blueprint $table){
            $table->increments('id');
            $table->string('table_name');
            $table->integer('foreign_id');
            $table->integer('old_status');
            $table->integer('new_status');
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->string('comment');
            $table->timestamps();
        });
*/
