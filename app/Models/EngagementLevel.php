<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EngagementLevel extends Model
{
    public $table = "engagement_levels";
}
