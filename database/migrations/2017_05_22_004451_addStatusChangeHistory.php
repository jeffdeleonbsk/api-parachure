<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusChangeHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_change_history', function(Blueprint $table){
            $table->increments('id');
            $table->string('table_name');
            $table->integer('foreign_id');
            $table->integer('old_status');
            $table->integer('new_status');
            $table->integer('editor_id')->default(0);
            $table->integer('editor_entity_id')->default(0);
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_change_history');
    }
}
