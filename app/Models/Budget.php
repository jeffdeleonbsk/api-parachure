<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Budget extends Model
{
    public $table = "budgets";
    public static function addBudget($arr)
    {
        $tosave = new Budget();
        $tosave->table_name = $arr['table_name'];
        $tosave->foreign_id = $arr['foreign_id'];
        $tosave->name = $arr['name'];
        $tosave->json_content = $arr['json_content'];
        $tosave->status = 1;
        $tosave->save();    
    }       

    private static function getDebtForMonth(&$debts, $month){
    	$total = 0;
    	foreach( $debts as $debt){
    		if( $debt->months_to_pay >= $month ){
    			//$monthly_amount = floatval($debt->total_amount)/floatval($debt->months_to_pay);
    			$total += $debt->monthly_amount;
    		}
    	}
    	return $total;
    }
    private static function getTotalDebt(&$debts){
        $total = 0;
        foreach( $debts as $debt){
            $total += $debt->total_amount;
        }
        return $total;
    }
	private static function getMonthlyExpenses(&$expenses){
    	$total = 0;
    	foreach( $expenses as $expense){
    		$total += $expense->amount;
    	}
    	return $total;
    }
    public static function getAtAge(&$out, $age)
    {
        foreach($out as $m){
            if( $m['age'] == $age ){
                return $m;
            }
        }
        return null;
    }
    public static function compute($inp)
    {
    	$monthly_interest = log10( 1 + ($inp->user->interest)/100.0) / 12.0;
    	$monthly_interest = pow(10, $monthly_interest);
    	$annual_inflation = 1 + ($inp->user->inflation)/100.0;
		$monthly_inflation = log10( $annual_inflation ) / 12.0;
    	$monthly_inflation = pow(10, $monthly_inflation);
    	$out = [];

    	$year = 1;
    	$month = 1;
    	$expenses_amt =  self::getMonthlyExpenses($inp->expenses);
    	$income =  $inp->user->income;
    	$mf = 0;
    	$months_to_target = (101 - intval($inp->user->age)) * 12;
    	$savings_not_invested = 0;
        $currAge = $inp->user->age;
        $totalDebtPaid = 0;
        $totalDebts = self::getTotalDebt($inp->debts);
        $toInvest = 0;
    	for( $ind = 1 ; $ind <= $months_to_target ; $ind++){
    		if( $month > 12){
    			$year += 1;
                $currAge += 1;
    			$month = 1;
    			$expenses_amt = $expenses_amt * $annual_inflation;
    			$income = $income * $annual_inflation;
    		}
    		$debt = self::getDebtForMonth($inp->debts, $ind);

    		$disposable = $income-($expenses_amt+$debt);
            if( $disposable > 0 ){
                 $toInvest += $disposable;
            }
           

    		$savings_not_invested += $disposable; 
    		$old_mf = $mf;
    		$new_mf = $mf * $monthly_interest;
    		$rev_from_interest = $new_mf - $old_mf;

            $addedToInvestment = 0;

            if( $toInvest >= 5000 ){
                $mf = $new_mf + $toInvest;
                 $addedToInvestment = $toInvest;
                $toInvest = 0;
            }
    		if( $debt > 0 ){
                $totalDebtPaid += $debt;
            }
            
            $remaining_debt = $totalDebts - $totalDebtPaid;
            if( abs($remaining_debt) < 0.01 )
                $remaining_debt = 0;
    		$out[] = [
                'age' => $currAge,
    			'year' => $year,
    			'month' => $month,
    			'income' =>$income,
    			'expenses' => $expenses_amt,
    			'disposable'=>$disposable,
    			'mf'=>$mf,
                'added_to_investment'=> $addedToInvestment,
    			'rev_from_interest'=>$rev_from_interest,
    			'savings_if_not_invested'=>$savings_not_invested,                
                'debt_amount_paid' => $debt,
                'remaining_debt' => $remaining_debt ,
                'total_debt_paid'=>$totalDebtPaid,
                'totalDebt'=>$totalDebts,
                'addedToInvestment'=> $addedToInvestment,
                'toInvest'=> $toInvest
    		];
    		$month++;
    	}
        $targetAge = $inp->user->targetAge;
        $retirementAge = $inp->user->retirementAge;
        $targetAt30 = self::getAtAge($out, $targetAge);
        $targetAt60 = self::getAtAge($out, $retirementAge);
        $status = "positive";
        if( $targetAt30['savings_if_not_invested'] < 0 ){
            $status = 'negative';
        }
        $result = [
            "status"=>$status,
            "targetAt30"=>$targetAt30,
            "targetAt60"=>$targetAt60,
            "out"=>$out
        ];


    	return $result;
    	    	

    }
}
